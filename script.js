/*Теоретичні питання
1) Які існують типи даних у Javascript?
2) У чому різниця між == і ===?
3) Що таке оператор?*/

// 1) У JS 8 типiв: Number, String, Boolean, Undefined, BigInt, Symbol, Null, Object
// 2) При == JS намагається привести значення до одного типу та порiвняти, а при === JS не приводить до одного типу
// а відразу порiвнює, так сказати "сурове порівняння"
// 3) Оператор - знаки або вирази, що роблять якісь операції з операндами(змінні, тощо), називаються операторами





let firstName = prompt("Enter your name")
while (firstName === "" || firstName === null) {
    firstName = prompt("Enter your name again")
}

let age = +prompt("Enter your age")
while (Number.isNaN(+age))  {
    age = +prompt("Enter your age again")
}

if (age < 18) {
    alert("You are not allowed to visit this website")
} else if (age >= 18 && age <= 22) {
    let result = confirm("Are you sure you want to continue?")
    if (result) {
        alert(`Welcome, ${firstName}`)
    } else {
        alert("You are not allowed to visit this website")
    }
} else if (age > 22) {
    alert(`Welcome ${firstName}`)
}

